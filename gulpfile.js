var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var watch = require('gulp-watch');

/*gulp.task('game', function() {
    browserify(['./src/game/game.js'])
        .bundle()
        .pipe(source('game.js'))
        //.pipe(uglify())
        .pipe(gulp.dest('./public/javascripts/'));
});*/

gulp.task('stats', function() {
   return gulp.src('src/statistics.js')
       .pipe(gulp.dest('./public/javascripts'));
});

gulp.task('watch2', function() {
    watch('./src/statistics.js', function() {
        gulp.start('stats');
    });
});

gulp.task('game', function() {
    return gulp.src('src/game/*.js')
        .pipe(concat('game.js'))
        .pipe(gulp.dest('./public/javascripts/'));
});

gulp.task('watch', function() {
    watch('./src/game/*', function() {
        gulp.start('game');
    });
});
