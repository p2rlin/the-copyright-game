var express = require('express');
var router = express.Router();

var db = require("../db");

/* GET home page. */
router.get('/', function(req, res, next) {
    var data = [];
    var correctAnswersGeneral = db.getCorrectAnsweredGeneralPercentage();
    var answeredQuestions = correctAnswersGeneral.then(function(resultA) {
        data.push(resultA);
        return db.getQuestionsAnswered();
    });
    var playedGames = answeredQuestions.then(function(resultB) {
        data.push(resultB);
        return db.getNumberOfGames();
    });
    var correctAnswerPercentageMusic = playedGames.then(function(resultD) {
        data.push(resultD);
        return db.getCorrectAnsweredCategory('music');
    });
    var correctAnswerPercentageArt = correctAnswerPercentageMusic.then(function(resultE) {
        data.push(resultE);
        return db.getCorrectAnsweredCategory('art');
    });
    var correctAnswerPercentageTech = correctAnswerPercentageArt.then(function(resultF) {
        data.push(resultF);
        return db.getCorrectAnsweredCategory('tech');
    });
    var correctAnswerPercentageMisc = correctAnswerPercentageTech.then(function(resultG) {
        data.push(resultG);
        return db.getCorrectAnsweredCategory('misc');
    });
    var correctAnswerPercentageFilm = correctAnswerPercentageMisc.then(function(resultH) {
        data.push(resultH);
        return  db.getCorrectAnsweredCategory('film');
    });
    var correctAnswerPercentageLiterature = correctAnswerPercentageFilm.then(function(resultI) {
        data.push(resultI);
        return  db.getCorrectAnsweredCategory('literature');
    });
    correctAnswerPercentageLiterature.then(function(resultJ) {
        data.push(resultJ);
        var dataString = JSON.stringify(data);
        res.render('statistics', { title: 'Statistics', page: 'statistics', data: dataString });
    })
});

module.exports = router;
