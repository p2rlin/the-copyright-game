
var db = require('./db');
var io;

var games = {};


module.exports = function(server) {
    io = require('socket.io')(server);
    console.log('socket module loaded');

    io.on('connection', function(socket) {
        socket.on('join', function(data) {
            var player = new Player(socket);
            var game = null;
            for (let i in games) {
                if (games[i].isJoinable) {
                    game = games[i];
                    break;
                }
            }
            if (game == null) {
                game = new Game();
                games[game.hash] = game;
            }
            
            socket.join(game.hash);
            game.join(player); 

            socket.emit('game found', {gameHash: game.hash, availableKnobs: game.availableKnobs});
        });

        socket.on('disconnect', function(data) {
            let game = findPlayerGame(socket.id);
            if (game != null) {
                let player = game.removePlayer(socket.id);
                if (player.knobId != null) {
                    socket.broadcast.to(game.hash).emit('player disconnected', socket.id);
                }
            }
        });

        socket.on('knob chosen', function(data) {
            let players = [];
            let game = games[data.gameHash];
            let index;
            for (var p = 0; p < game.players.length; p++) {
                let player = game.players[p];
                if (player.socket.id == socket.id) {
                    player.knobId = data.knobId;
                    index = p;
                }
                players.push({knobId: player.knobId, id: player.socket.id});
            }
            for (let i = 0; i < game.availableKnobs.length; i++) {
                if (data.knobId == game.availableKnobs[i]) {
                    game.availableKnobs.splice(i, 1);
                } 
            }

            socket.broadcast.to(game.hash).emit('player joined', {id: socket.id, knobId: data.knobId, index: index});
            socket.emit('players list', players);
            if (game.players.length == 4) {
                io.in(game.hash).emit('start game', game.players[0].socket.id);
            }
        });

        socket.on('player ready', function(playerId) {
            let game = findPlayerGame(playerId);
            let player = game.findPlayer(playerId);
            player.isReady = true;
            if (game.getReadyCount() >= 2) {
                game.isJoinable = false;
                io.in(game.hash).emit('start game', game.players[0].socket.id);
            }
        });

        socket.on('dice rolled', function(gameId) {
            io.in(games[gameId].hash).emit('dice value', rollDice());
        });

        socket.on('player move', function(data) {
            io.in(data.gameId).emit('player move', {distance: data.distance, direction: data.direction});
        });

        socket.on('category choice', function(data) {
            let game = findPlayerGame(socket.id);
            db.getQuestion(data.category)
                .then(q => db.getAnswers(q))
                .then(d => {
                    var question = {
                        question: d.question,
                        correctInformation: d.correctInformation,
                        answers: d.answers,
                        correctAnswer: d.correctAnswer,
                        category: data.category
                    };
                    game.setQuestion(question);
                    io.in(data.gameId).emit('new question', question);
                });
        });

        socket.on('question answered', function(data) {
            let game = games[data.gameId];
            db.userAnswered(game.question.question, game.question.answers[data.index], data.gameId, game.question.correctAnswer == data.index ? 1 : 0, game.question.category);
            io.in(data.gameId).emit('question answered', {index: data.index});
        });

        socket.on('change turn', function(data) {
            let player = games[data.gameId].changeTurn(socket.id);
            io.in(data.gameId).emit('change turn', player.socket.id);
        });

        socket.on('winner announcement', function(gameId) {
            console.log('winner announcement', socket.id);
            io.in(gameId).emit('winner announcement', socket.id);
        });
    }); 
};

function findPlayerGame(playerId) {
    for (let i in games) {
        for (let j = 0; j < games[i].players.length; j++) {
            if (games[i].players[j].socket.id == playerId) {
                return games[i];
            }
        }
    }
    return null;
}

function rollDice() {
    return Math.floor(Math.random() * 6) + 1;
}

class Player {
    constructor(socket) {
        this.socket = socket;
        this.knobId = null;
        this.isReady = false;
    }
}

class Game {
    constructor() {
        this.hash = this.generateHash();
        this.isJoinable = true;
        this.players = [];
        this.availableKnobs = [0, 1, 2, 3];
        this.question = null;
    } 

    join(player) {
        this.players.push(player);
        if (this.players.length == 4) {
            this.isJoinable = false;
        }
    }

    removePlayer(playerId) {
        let player = null;
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].socket.id == playerId) {
                player = this.players[i];
                if (this.players[i].knobId != null) {
                    this.availableKnobs.push(player.knobId);
                }
                this.players.splice(i, 1);
            }
        }
        return player;
    }

    findPlayer(playerId) {
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].socket.id == playerId) {
                return this.players[i];
            }
        }
        return null;
    }

    getReadyCount() {
        let count = 0;
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].isReady) {
                count++;
            }
        }
        return count;
    }

    changeTurn(currentPlayerId) {
        let nextPlayer = null;
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].socket.id == currentPlayerId) {
                if (i == this.players.length - 1) {
                    nextPlayer = this.players[0];
                } else {
                    nextPlayer = this.players[i+1];
                }
            }
        }
        return nextPlayer;
    }

    setQuestion(question) {
        this.question = question;
    }

    generateHash() {
        let hash = '';
        for (let i = 0; i < 10; i++) {
            hash += Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return hash;
    }
}

