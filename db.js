function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

//Creating a database
var sqlite3 = require('sqlite3').verbose();
var db = new sqlite3.Database('db.sqlite3');
var check;


module.exports = {
    userAnswered: function (question, answer, gameid, isCorrect, categoryType) {
        db.run("INSERT INTO statistics VALUES ('" + question + "', '" + answer + "', '" + gameid + "', '" + isCorrect + "', '" + categoryType + "' )");
    },
    getCorrectAnsweredCategory: function (category) {
        return new Promise((resolve, reject) => {
             db.get("SELECT isCorrect, (Count(isCorrect)* 100 / (SELECT Count(*) FROM statistics WHERE category='"+category+"')) AS Score  FROM statistics WHERE isCorrect=1  AND category='" + category + "' GROUP BY  isCorrect;", (err, result) => {                    if (err) {
                        return reject(err)
                    }

                    if (result === undefined) {
                        return resolve(0);
                    } else {
                        return resolve(result.Score);
                    }
                }
            )
        })
    },
    getCorrectAnsweredGeneralPercentage: function () {
        return new Promise((resolve, reject) => {
            db.each("SELECT isCorrect, (Count(isCorrect)* 100 / (SELECT Count(*) FROM statistics)) AS Score  FROM statistics WHERE isCorrect=1 GROUP BY  isCorrect;", (err, result) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(result.Score)
                }
            )
        })
    },
    getNumberOfGames: function () {
        return new Promise((resolve, reject) => {
            db.each("SELECT COUNT(DISTINCT game_id) AS numberof FROM statistics;", (err, result) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(result.numberof)
                }
            )
        })
    },
    getQuestionsAnswered: function () {
        return new Promise((resolve, reject) => {
            db.each("SELECT COUNT(*) AS numberof FROM statistics;", (err, result) => {
                    if (err) {
                        return reject(err)
                    }
                    return resolve(result.numberof)
                }
            )
        })
    },
    getQuestion: function (category_name) {
        return new Promise((resolve, reject) => {
            db.get("SELECT * FROM question WHERE category_name='" + category_name + "' ORDER BY RANDOM() LIMIT 1;", (err, result) => {
                if (err) {
                    return reject(err)
                }
                return resolve(result)
            });
        });
    },
    getAnswers: function (question) {
        console.log(question);
        return new Promise((resolve, reject) => {
            db.all("SELECT * FROM answer WHERE question='" + question.question + "';", (err, result) => {
                if (err) {
                    return reject(err);
                }

                let answers = [];
                let correct_index = -1;

                result = shuffle(result);

                for (var i = 0; i < result.length; i++) {
                    if (result[i].is_correct) {
                        correct_index = i;
                    }
                    answers.push(result[i].answer);
                }

                return resolve({
                    question: question.question,
                    correctInformation: question.correct_information,
                    answers: answers,
                    correctAnswer: correct_index
                });
            });
        });
    },
    closeDB: function () {
        db.close();
    }
}

