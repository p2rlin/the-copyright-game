var TILE = {
    "width": 75,
    "height": 75,
    "margin": 15,
    "order": [
        [0, 4, 2, 1, 5, 6, 3, 0],
        [1,                   5], // note: 2 elements
        [2,                   3],
        [3,                   1],
        [4,                   6],
        [5,                   2],
        [6,                   4],
        [0, 2, 6, 3, 5, 4, 1, 0]
    ],
    "type": {
        0: "copyright_tile",
        1: "misc_tile",
        2: "music_tile",
        3: "art_tile",
        4: "film_tile",
        5: "literature_tile",
        6: "tech_tile",
    },
    "color": {
        0: 0xf9f9f9,
        1: 0x800099,
        2: 0xec0066,
        3: 0xaad400,
        4: 0x00aa88,
        5: 0x0055d4,
        6: 0xff7f2a
    }
};


class Board {
    constructor(stage) {
        this.list = [];
        this.container = new PIXI.Container(); 
        this.tiles = new PIXI.Container();

        this.createList();
        this.createTiles();
        this.container.addChild(this.tiles);
        Utils.alignCenter(this.container, game.renderer.view);

        this.playerTurnText = null;

        this.dice = new Dice(this);

        stage.addChild(this.container);
    };

    createTiles() {
        for (var i = 0; i < TILE.order.length; i++) {
            for (var j = 0; j < TILE.order[i].length; j++) {
                if (i > 0 && i < TILE.order.length-1 && j > 0) {
                    j = TILE.order.length - 1;
                    var type = TILE.type[TILE.order[i][1]];
                } else {
                    var type = TILE.type[TILE.order[i][j]];
                }

                let tile = Utils.createSprite(IMAGES_PATH + type + ".png",
                            { width: TILE.width, height: TILE.height, 
                              x: TILE.width*j + (j == 0 ? 0 : j*TILE.margin),
                              y: TILE.height*i + (i == 0 ? 0 : i*TILE.margin) }
                );
                this.tiles.addChild(tile);
            }
        }
    }

    createCategoryChoice(diceValue) {
        var wrapper = Utils.createText("CHOOSE CATEGORY", 14, "#333", { fontWeight: "bold" });
        Utils.alignCenter(wrapper, this.container);
        wrapper.y += 50;

        var player = game.playerTurn;
        if (diceValue != -1) {
            var types = [this.getTileType(player.currentTile - diceValue), this.getTileType(player.currentTile + diceValue)];
        } else {
            var types = [1,2,3,4,5,6];
        }

        var choices = new PIXI.Container();
        choices.y = 20 + wrapper.height;

        for (let i = 0; i < types.length; i++) {
            let choice = Utils.drawCircle((15*2+10)*i, 0, 15, TILE.color[types[i]],
                                { interactive: true }
            );

            var _this = this;
            choice.click = function(mouseData) {
                if (types.length == 2) {
                    socket.emit('player move', { gameId: game.hash, distance: diceValue, direction: i == 0 ? DIRECTION.LEFT : DIRECTION.RIGHT});
                    if (types[i] == 0) {
                        _this.createCategoryChoice(-1);
                    } else {
                        socket.emit('category choice', { gameId: game.hash, category: TILE.type[types[i]].split("_")[0] });
                    }
                } else {
                    let nearestTile = _this.findNearestTile(i + 1, player);
                    socket.emit('player move', { gameId: game.hash, distance: nearestTile.distance, direction: nearestTile.direction});
                    socket.emit('category choice', { gameId: game.hash, category: TILE.type[types[i]].split("_")[0] });
                }
                wrapper.destroy();
            };
            choices.addChild(choice);
        }

        choices.x = wrapper.width / 2 - choices.width / 2 + 15;
        wrapper.addChild(choices);

        this.container.addChild(wrapper);
    }

    findNearestTile(type, player) {
        var best = 9999999;
        for (let i = 0; i < this.list.length; i++) {
            if (this.list[i] == type) {
                if (Math.abs(best) > Math.abs(player.currentTile - i)) {
                    best = player.currentTile - i;
                }
                if (player.currentTile == 0 && Math.abs(best) > Math.abs(this.list.length - i)) {
                    best = this.list.length - i;
                }
            }
        }
        return { "type": type, "distance": Math.abs(best), "direction": (best > 0 ? DIRECTION.LEFT : DIRECTION.RIGHT) };
    }

    getTileType(index) {
        if (index < 0) {
            index += this.list.length;
        } else if (index >= this.list.length) {
            index -= this.list.length;
        }
        return this.list[index];
    }

    setPlayerTurnText() {
        let color = game.playerTurn.knob.texture.baseTexture.imageUrl.split("/")[1].split("_")[0].toUpperCase();
        if (this.playerTurnText == null) {
            this.playerTurnText = Utils.createText(color + " PLAYS", 18, "#f9f9f9");
            this.container.addChild(this.playerTurnText);
        } else {
            this.playerTurnText.text = color + " PLAYS";
        }
        Utils.alignCenter(this.playerTurnText, this.container);
        this.playerTurnText.y -= 150;

    }

    createList() {
        for (let i = 0; i < TILE.order[0].length; i++) {
            let tile = TILE.order[0][i];
            this.list.push(tile);
        }

        for (let i = 1; i < TILE.order.length - 1; i++) {
            let tile = TILE.order[i][1];
            this.list.push(tile);
        }

        for (let i = TILE.order[TILE.order.length - 1].length - 1; i > 0; i--) {
            let tile = TILE.order[TILE.order.length - 1][i];
            this.list.push(tile);
        }

        for (let i = TILE.order.length - 1; i >= 1; i--) {
            let tile = TILE.order[i][0];
            this.list.push(tile);
        }
    }
}

