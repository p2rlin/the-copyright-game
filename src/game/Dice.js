class Dice {
    constructor(board) {
        this.container = null;
        this.board = board;
        this.DICE_TEXT = "ROLL DICE";
    }
    
    click(mouseData) {
        if (game.board.dice.container.text != undefined) {
            socket.emit('dice rolled', game.hash);
        }
    }

    makeClickable() {
        if (this.container) {
            this.container.interactive = true;
            this.container.buttonMode = true;
            this.container.click = this.click;
            if (game.playerTurn.id != socket.id) {
                this.board.dice.container.interactive = false;
            }
        }
    }

    setText(text, fontSize) {
        if (this.container) {
            this.container.destroy();
        }
        if (game.playerTurn.id == socket.id) {
            this.container = Utils.createText(text ? text : this.DICE_TEXT, fontSize ? fontSize : 18, "#f9f9f9", {fontWeight: "bold"});
            Utils.alignCenter(this.container, this.board.container);
            this.board.container.addChild(this.container);
        }
    }

    setSprite(number) {
        if (this.container) {
            this.container.destroy();
        }
        this.container = Utils.createSprite(IMAGES_PATH + 'dice' + number + '.png');
        Utils.alignCenter(this.container, this.board.container);
        this.board.container.addChild(this.container);
        this.board.dice.container.interactive = false;
    }

    show() {
        if (this.container && game.hasStarted == true)
            this.container.visible = true;
    }

    hide() {
        if (this.container)
            this.container.visible = false;
    }
}

