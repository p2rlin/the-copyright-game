var DIRECTION = {
    "RIGHT": 1,
    "LEFT": 0
};

var PLAYER_KNOB_TYPE = {
    0: "red_knob",
    1: "green_knob",
    2: "blue_knob",
    3: "yellow_knob",
};

class Player {
    constructor(index, id, knobId, board) {
        this.startTile = [0,7,14,21];
        this.knobOffset = {
            0: {"x": 2, "y": 25},
            1: {"x": 16, "y": 25},
            2: {"x": 32, "y": 25},
            3: {"x": 48, "y": 25},
        };
        this.startPos = {
            0: this.knobOffset[index],
            1: {"x": this.knobOffset[index].x + 7*(TILE.width + TILE.margin), "y": this.knobOffset[index].y},
            2: {"x": this.knobOffset[index].x + 7*(TILE.width + TILE.margin), "y": this.knobOffset[index].y + 7*(TILE.height + TILE.margin)},
            3: {"x": this.knobOffset[index].x, "y": this.knobOffset[index].y + 7*(TILE.height + TILE.margin)}
        };
        this.smallCirclePos = null;
        this.id = id;
        this.knobId = knobId;
        this.index = index;
        this.knob = this.initKnob();
        this.smallCircles = [];
        this.profile = this.createProfile();
        this.currentTile = this.startTile[index];
        this.step = 10;
        this.moving = false;
        this.tilesToMove = 0;
        this.answeredCorrectly = [1, 2, 3, 4, 5];

        board.addChild(this.knob);
    };

    initKnob() {
        let k = new PIXI.Sprite(
            PIXI.loader.resources[IMAGES_PATH + PLAYER_KNOB_TYPE[this.knobId] + ".png"].texture
        );
        k.x = this.startPos[this.index].x;
        k.y = this.startPos[this.index].y;
        return k;
    }

    createProfile() {
        let profile = new PIXI.Container();
        let circle = Utils.drawCircle(35, 35, 35, 0x333333);

        switch(this.index) { 
            case 0:
                profile.x = game.board.container.x - 150;
                profile.y = game.board.container.y;
                break;
            case 1:
                profile.x = game.board.container.x + game.board.container.width + 150 - circle.width;
                profile.y = game.board.container.y;
                break;
            case 2:
                profile.x = game.board.container.x + game.board.container.width + 150 - circle.width;
                profile.y = game.board.container.y + game.board.container.height - circle.height;
                break;
            case 3:
                profile.x = game.board.container.x - 150;
                profile.y = game.board.container.y + game.board.container.height - circle.height;
                break;
        }

        profile.addChild(circle);

        let knob = Utils.createSprite(IMAGES_PATH + PLAYER_KNOB_TYPE[this.knobId] + ".png", { alpha: 0.5 });
        Utils.alignCenter(knob, profile);
        profile.addChild(knob);

        this.smallCirclePos = [
            [0, profile.height/2],
            [profile.width, profile.height/2],
            [profile.width/2-20, 5],
            [profile.width/2+20, 5],
            [profile.width/2-20, profile.height-5],
            [profile.width/2+20, profile.height-5],
        ];

        for (let i = 0; i < this.smallCirclePos.length; i++) {
            let smallCircle = Utils.drawCircle(this.smallCirclePos[i][0], this.smallCirclePos[i][1], 10, 0xb3b3b3);
            profile.addChild(smallCircle);
            this.smallCircles.push(smallCircle);
        }

        game.stage.addChild(profile);
        return profile;
    }

    award(categoryType) {
        if (!(this.answeredCorrectly.indexOf(categoryType) > -1)) {
            let index = this.answeredCorrectly.length;
            this.smallCircles[index].clear();
            this.smallCircles[index].beginFill(TILE.color[categoryType]);
            this.smallCircles[index].drawCircle(this.smallCirclePos[index][0], this.smallCirclePos[index][1], 10);
            this.answeredCorrectly.push(categoryType);
            if (this.answeredCorrectly.length == 6) {
                socket.emit('winner announcement', game.hash);
            }
        }
    }

    move(number, direction) {
        this.direction = direction;
        this.moving = true;
        this.tilesToMove = number;
    }

    updatePosition() {
        if (this.moving && this.tilesToMove > 0) {
            if (this.direction == DIRECTION.LEFT) {
                this.moveBackward();
            } else {
                this.moveForward();
            }
        } else {
            this.moving = false;
        }
    }

    moveForward() {
        if (this.knob.x < this.startPos[1].x && this.knob.y == this.startPos[1].y) {
            this.moveRight();
        } else if (this.knob.x == this.startPos[2].x && this.knob.y < this.startPos[2].y){
            this.moveDown();
        } else if (this.knob.x > this.startPos[3].x && this.knob.y == this.startPos[3].y) {
            this.moveLeft();
        } else {
            this.moveUp();
        }
    }

    moveBackward() {
        if (this.knob.x == this.startPos[3].x && this.knob.y < this.startPos[3].y) {
            this.moveDown();
        } else if (this.knob.x < this.startPos[2].x && this.knob.y == this.startPos[2].y){
            this.moveRight();
        } else if (this.knob.x == this.startPos[1].x && this.knob.y > this.startPos[1].y) {
            this.moveUp();
        } else {
            this.moveLeft();
        }
    }

    moveRight() {
        this.knob.x += this.step;
        if (this.knob.x % (TILE.width + TILE.margin) == this.knobOffset[this.index].x) {
            this.tilesToMove -=1;
            this.updateCurrentTile();
        }
    }

    moveLeft() {
        this.knob.x -= this.step;
        if (this.knob.x % (TILE.width + TILE.margin) == this.knobOffset[this.index].x) {
            this.tilesToMove -=1;
            this.updateCurrentTile();
        }
    }

    moveDown() {
        this.knob.y += this.step;
        if (this.knob.y % (TILE.width + TILE.margin) == this.knobOffset[this.index].y) {
            this.tilesToMove -=1;
            this.updateCurrentTile();
        }
    }

    moveUp() {
        this.knob.y -= this.step;
        if (this.knob.y % (TILE.width + TILE.margin) == this.knobOffset[this.index].y) {
            this.tilesToMove -=1;
            this.updateCurrentTile();
        }
    }

    updateCurrentTile() {
        if (this.direction == DIRECTION.RIGHT) {
            this.currentTile = this.currentTile + 1;
        } else {
            this.currentTile = this.currentTile -1;
        }
        if (this.currentTile > game.board.list.length - 1) {
            this.currentTile -= game.board.list.length;
        } else if (this.currentTile < 0) {
            this.currentTile += game.board.list.length;
        }
    }
}

