class Question {
    constructor(text, answers, correctIndex, correctInformation, categoryType, card) {
        this.text = text;
        this.answers = answers;
        this.correctIndex = correctIndex;
        this.correctInformation = correctInformation;
        this.categoryType = categoryType;
        this.card = card;

        this.width = 320;
        this.height = 124;

        this.container = this.createSprite();
    }

    createSprite() {
        let question = Utils.createText(this.text, 12, "#333", { x: 15, y: 15, wordWrapWidth: this.width, fontWeight: "bold" });
        for (let i = 0; i < this.answers.length; i++) {
            question.addChild(this.createAnswer(i, this.answers[i], question));
        }
        return question;
    }

    getCorrectSprite() {
        return Utils.createText(this.correctInformation, 12, "#333", { x: 15, y: 15, wordWrapWidth: this.width });
    }

    createAnswer(index, text, question) {
        let interactive = false;
        if (game.playerTurn.id == socket.id) {
            interactive = true;
        }
        let answer = Utils.createText((index+1) + ". " + text, 12, "#333", { x: 15, y: question.height + 10 + index * 18, wordWrapWidth: this.width, interactive: interactive });

        var _this = this;
        answer.click = function(mouseData) {
            socket.emit('question answered', {gameId: game.hash, index: index});
        };
        return answer;
    }

    isCorrect(choice) {
        return choice == this.correctIndex;
    }
}
