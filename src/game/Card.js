var CARD = {
    "type": {
        0: "blank_card",
        1: "misc_card",
        2: "music_card",
        3: "art_card",
        4: "film_card",
        5: "literature_card",
        6: "tech_card",
    }
};

class Card {
    constructor(board) {
        this.board = board;
        this.question = null;
        this.container = new PIXI.Sprite();
        this.container.visible = false; 
        this.container.width = 350;
        this.container.height = 158;
        Utils.alignCenter(this.container, board.container);

        board.container.addChild(this.container);
    }

    setQuestion(question) {
        this.question = question;
        this.container.texture = PIXI.loader.resources[IMAGES_PATH + CARD.type[question.categoryType] + ".png"].texture;
        this.container.visible = true;
    }

    showQuestion() {
        var _this = this;
        setTimeout(function() {
            _this.container.texture = PIXI.loader.resources[IMAGES_PATH + CARD.type[0] + ".png"].texture;
            _this.container.addChild(_this.question.container);
        }, 1000);
    }

    showCorrectInformation() {
        var _this = this;
        setTimeout(function() {
            _this.question.container.destroy({children: true});
            _this.question.container = _this.question.getCorrectSprite();
            _this.container.addChild(_this.question.container);
            if (game.playerTurn.id == socket.id) {
                let continueBtn = _this.createContinueButton();
                _this.container.addChild(continueBtn);
            }
        }, 1000);
    }

    createContinueButton() {
        let button = Utils.drawRect(0, 0, 150, 30, TILE.color[this.question.categoryType], { interactive: true });
        button.position.set(this.container.width - button.width - 15, this.container.height - button.height - 15);

        let text = Utils.createText("CONTINUE", 14, "#FFF", { interactive: true, fontWeight: "bold" });
        Utils.alignCenter(text, button);
        button.addChild(text);

        let _this = this;
        button.click = function(mouseData) {
            if (game.hasStarted) {
                _this.container.visible = false;
            }
            socket.emit('change turn', {gameId: game.hash});
            button.destroy();
        };

        return button;
    }

    flip() {
        //TODO: add flip animation if possible

    }
}
