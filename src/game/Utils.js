class Utils {
    static createText(textValue, fontSize, color, opts) {
        if (!opts) opts = {};
        let text = new PIXI.Text(textValue, opts);
        text.style.fontSize = fontSize;
        text.style.fill = color ? color : 0x000000;
        text.buttonMode = opts.interactive;
        text.interactive = opts.interactive;
        text.x = opts.x ? opts.x : 0;
        text.y = opts.y ? opts.y : 0;
        text.style.wordWrap = opts.wordWrapWidth ? true : false;
        text.style.fontFamily = "sans-serif";
        return text;
    }

    static createSprite(res, opts) {
        if (!opts) opts = {};
        let sprite = new PIXI.Sprite(PIXI.loader.resources[res].texture);
        sprite.x = opts.x ? opts.x : 0;
        sprite.y = opts.y ? opts.y : 0;
        if (opts.width) sprite.width = opts.width;
        if (opts.height) sprite.height = opts.height;
        sprite.interactive = opts.interactive;
        sprite.buttonMode = opts.interactive;
        sprite.alpha = opts.alpha ? opts.alpha : 1;
        return sprite;
    }

    static drawRect(x, y, w, h, color, opts) {
        if (!opts) opts = {};
        let rect = new PIXI.Graphics();
        rect.beginFill(color);
        rect.drawRect(x, y, w, h);
        rect.endFill();
        rect.interactive = opts.interactive;
        rect.buttonMode = opts.interactive;
        rect.defaultCursor = opts.cursor;
        rect.defaultCursor = opts.interactive ? "pointer" : "default";
        return rect;
    }

    static drawCircle(x, y, r, color, opts) {
        if (!opts) opts = {};
        let circle = new PIXI.Graphics();
        circle.beginFill(color);
        circle.drawCircle(x, y, r);
        circle.endFill();
        circle.interactive = opts.interactive;
        circle.buttonMode = opts.interactive;
        circle.defaultCursor = opts.interactive ? "pointer" : "default";
        return circle;
    }

    static alignCenter(s1, s2) {
        s1.x = s2.width/2 - s1.width/2;
        s1.y = s2.height/2 - s1.height/2;
    }
}
