var IMAGES_PATH = "images/";


class Game {
    constructor() {
        this.hash = null;

        this.renderer = PIXI.autoDetectRenderer(
            1200, 800,
            {transparent: true}
        );
        document.getElementById("container").appendChild(this.renderer.view);

        this.hasStarted = false;
        this.stage = new PIXI.Container();
        this.statusText = null;
        this.players = [];
        this.playerTurn = null;

        this.board = null;
        this.questionCard = null;
        this.init();
    }

    init() {
        var cb = (function () { // called after PIXI.loader has finished loading
            this.board = new Board(this.stage);
            this.questionCard = new Card(this.board);
            this.join();

            this.gameLoop();
        }).bind(this);

        PIXI.loader
            .add([
                IMAGES_PATH + "copyright_tile.png",
                IMAGES_PATH + "tech_tile.png",
                IMAGES_PATH + "literature_tile.png",
                IMAGES_PATH + "film_tile.png",
                IMAGES_PATH + "art_tile.png",
                IMAGES_PATH + "music_tile.png",
                IMAGES_PATH + "misc_tile.png",
            ])
            .add([
                IMAGES_PATH + "red_knob.png",
                IMAGES_PATH + "green_knob.png",
                IMAGES_PATH + "blue_knob.png",
                IMAGES_PATH + "yellow_knob.png"
            ])
            .add([
                IMAGES_PATH + "blank_card.png",
                IMAGES_PATH + "tech_card.png",
                IMAGES_PATH + "literature_card.png",
                IMAGES_PATH + "film_card.png",
                IMAGES_PATH + "art_card.png",
                IMAGES_PATH + "music_card.png",
                IMAGES_PATH + "misc_card.png",
            ]).add([
                IMAGES_PATH + "dice1.png",
                IMAGES_PATH + "dice2.png",
                IMAGES_PATH + "dice3.png",
                IMAGES_PATH + "dice4.png",
                IMAGES_PATH + "dice5.png",
                IMAGES_PATH + "dice6.png",
            ])
            .load(cb);
    };

    gameLoop() {
        var _this = this;
        requestAnimationFrame(function () {
            _this.gameLoop();
        });

        this.update();

        for (let i = 0; i < this.players.length; i++) {
            this.players[i].updatePosition();
        }

        this.renderer.render(this.stage);
    }; 

    join() {
        this.statusText = Utils.createText(
            "FIND GAME", 18, "#f9f9f9",
            { fontWeight: "bold", interactive: true }
        );
        Utils.alignCenter(this.statusText, this.board.container);
        let _this = this;
        this.statusText.click = function(mouseData) {
            socket.emit('join', '');
            _this.statusText.interactive = false;
        };

        this.board.container.addChild(this.statusText);
    }

    chooseKnob(data) {
        this.hash = data.gameHash;

        this.statusText.text = "CHOOSE KNOB";
        Utils.alignCenter(this.statusText, this.board.container);
        
        var wrapper = new PIXI.Container();
        for (let i = 0; i < data.availableKnobs.length; i++) {
            let knobId = data.availableKnobs[i];
            let knob = Utils.createSprite(IMAGES_PATH + PLAYER_KNOB_TYPE[knobId] + ".png",
                                            { y: 0, interactive: true });
            knob.x = (knob.width+20)*i;

            var _this = this;
            knob.click = function(mouseData) {
                socket.emit('knob chosen', {knobId: knobId, gameHash: _this.hash});
                _this.board.container.removeChild(wrapper);
                wrapper.destroy();
                _this.playerIsReady();
            };

            wrapper.addChild(knob);
        }
        Utils.alignCenter(wrapper, this.board.container);
        wrapper.y += this.statusText.height + 20;
        this.board.container.addChild(wrapper);
    }

    playerIsReady() {
        this.statusText.text = "I'M READY!";
        Utils.alignCenter(this.statusText, this.board.container);
        this.statusText.interactive = true;
        this.statusText.buttonMode = true;
        let _this = this;
        this.statusText.click = function(mouseData) {
            _this.statusText.interactive = false;
            _this.statusText.style.fontWeight = "normal";
            _this.statusText.text = "WAITING FOR OTHERS";
            Utils.alignCenter(_this.statusText, _this.board.container);
            socket.emit('player ready', socket.id);
        }
    }

    playerJoined(data) {
        this.players.push(new Player(data.index, data.id, data.knobId, this.board.container));
    }

    addPlayers(players) {
        for (let i = 0; i < players.length; i++) {
            let player = players[i];
            player.index = i;
            this.playerJoined(player);
        }
        this.playerTurn = this.players[0];
    }

    playerDisconnected(playerId) {
        let player = this.findPlayer(playerId);
        if (this.hasStarted) {
            player.knob.destroy();
            player.profile.children[1].destroy();
        } else {
            player.knob.destroy();
            player.profile.destroy({children: true});
        }
    }

    findPlayer(playerId) {
        for (let i = 0; i < this.players.length; i++) {
            if (this.players[i].id == playerId) {
                return this.players[i];
            }
        }
        return null;
    }

    start(playerId) {
        this.hasStarted = true;
        this.statusText.destroy();
        this.playerTurn = this.findPlayer(playerId);
        this.playerTurn.profile.children[1].alpha = 1;
        this.board.dice.setText();
        this.board.dice.makeClickable(); 
        this.board.dice.show();
        this.board.setPlayerTurnText();
    }

    setDiceValue(value) {
        this.board.dice.setSprite(value);
        this.board.dice.show();
        if (this.playerTurn.id == socket.id) {
            this.board.createCategoryChoice(value);
        }
    }

    movePlayer(data) {
        this.playerTurn.move(data.distance, data.direction);
    }

    newQuestion(data) {
        this.board.dice.hide();

        let categoryType = 0;
        for (let i in CARD.type) {
            if (CARD.type[i].indexOf(data.category) !== -1) {
                categoryType = i;
                break;
            }
        }

        let question = new Question(
            data.question,
            data.answers,
            data.correctAnswer,
            data.correctInformation,
            categoryType,
            this.questionCard
        );
        this.questionCard.setQuestion(question);
        this.questionCard.showQuestion();
    }

    questionAnswered(data) {
        let question = this.questionCard.question;
        if (question.isCorrect(data.index)) {
            question.container.children[data.index].style.fill = "#61B329";
            game.playerTurn.award(question.categoryType);
        } else {
            question.container.children[data.index].style.fill = "#ee0000";
        }
        this.questionCard.showCorrectInformation();
    }

    changeTurn(playerId) {
        if (this.hasStarted) {
            this.playerTurn = this.findPlayer(playerId);

            for (let i = 0; i < this.players.length; i++) {
                this.players[i].profile.children[1].alpha = 0.5;
            }
            this.playerTurn.profile.children[1].alpha = 1;

            this.board.dice.setText();
            this.board.dice.makeClickable();
            this.board.dice.show();

            this.board.setPlayerTurnText();
        }

        this.questionCard.container.visible = false;
        this.questionCard.question.container.destroy();

    }

    announceWinner(winnerId) {
        let color = game.playerTurn.knob.texture.baseTexture.imageUrl.split("/")[1].split("_")[0].toUpperCase();
        this.hasStarted = false;
        this.board.playerTurnText.text = color + " WINS!";
    }

    update() {

    };
}

var socket = io.connect('http://0.0.0.0:3000');
var game = new Game();

socket.on('game found', game.chooseKnob.bind(game));
socket.on('player joined', game.playerJoined.bind(game));
socket.on('players list', game.addPlayers.bind(game));
socket.on('player disconnected', game.playerDisconnected.bind(game));
socket.on('start game', game.start.bind(game));
socket.on('dice value', game.setDiceValue.bind(game));
socket.on('player move', game.movePlayer.bind(game));
socket.on('new question', game.newQuestion.bind(game));
socket.on('question answered', game.questionAnswered.bind(game));
socket.on('change turn', game.changeTurn.bind(game));
socket.on('winner announcement', game.announceWinner.bind(game));

