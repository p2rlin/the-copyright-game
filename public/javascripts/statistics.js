let COLORS = {
    "Music":            "stroke-color: #a50047; stroke-width: 2; fill-color: #ec0066",
    "Art":              "stroke-color: #769400; stroke-width: 2; fill-color: #aad400",
    "Technology":       "stroke-color: #b2581d; stroke-width: 2; fill-color: #ff7f2a",
    "Miscellaneous":    "stroke-color: #59006b; stroke-width: 2; fill-color: #800099",
    "Literature":       "stroke-color: #003b94; stroke-width: 2; fill-color: #0055d4",
    "Film":             "stroke-color: #00765f; stroke-width: 2; fill-color: #00aa88"
};

var DATA = {};

function getData() {
    return [
        ['Category',        'Percentage',       {role: 'style'}],
        ['Music',           DATA.music,         COLORS["Music"]],
        ['Art',             DATA.art,           COLORS["Art"]],
        ['Technology',      DATA.tech,          COLORS["Technology"]],
        ['Miscellaneous',   DATA.misc,          COLORS["Miscellaneous"]],
        ['Literature',      DATA.literature,    COLORS["Literature"]],
        ['Film',            DATA.film,          COLORS["Film"]],
    ];
}

function drawPieChart(data, options, div) {
    var chart = new google.visualization.PieChart(document.getElementById(div));
    chart.draw(data, options);
}

function drawColumnChart(data, options, div) {
    var chart = new google.visualization.ColumnChart(document.getElementById(div));
    chart.draw(data, options);
}

function drawBarChart(data, options, div) {
    var chart = new google.visualization.BarChart(document.getElementById(div));
    chart.draw(data, options);
}

function getDummyData1() {
    return [
        ['Answer', 'Percentage'],
        ['Yes', 15],
        ['No', 85]
    ];
}

function getDummyData2() {
    return [
        ['Answer','Percentage'],
        ['Yes', 25],
        ['No', 75]
    ];
}

function getDummyData3() {
    return [
        ['Answer','Percentage'],
        ['The copyright holder of the lyrics', 60],
        ['The copyright holder of the music', 15],
        ['The copyright holder of the sound recording', 15],
        ['All of them', 10],
    ];
}

function drawChart() {
    var list = getData();
    var data1 = google.visualization.arrayToDataTable(list);
    var data2 = google.visualization.arrayToDataTable(getDummyData1());
    var data3 = google.visualization.arrayToDataTable(getDummyData2());
    var data4 = google.visualization.arrayToDataTable(getDummyData3());

    var options = {'title':'Correct answers % by category',
        'width': 1100,
        'height': 600,
        'backgroundColor': {
            'fill': 'transparent'
        },
        legend: 'none',
        titleTextStyle: {
            color: '#fff',
            fontSize: 20
        },
        hAxis: {
            baselineColor: '#fff',
            textStyle:{color: '#FFF'},
        },
        vAxis: {
            baselineColor: '#fff',
            textStyle:{color: '#FFF'},
        }
    };

    drawBarChart(data1, options, "chart1");

    options.title = "Alice took a photo of her cat with her phone. Does the photo get automatic copyright protection?";
    drawPieChart(data2, options, "chart2");
    options.title = "Can a genetically modified animal be patented?";
    drawPieChart(data3, options, "chart3");
    options.title = "Peter wants to use music protected by copyright as a soundtrack for a home video that he made and upload it to YouTube. Whose permission does he need?";
    drawPieChart(data4, options, "chart4");
}

function createChart(data) {
    var correctPercent = data[0];
    var answeredQuestions = data[1];
    var playedGames = data[2];
    DATA.music = data[3];
    DATA.art = data[4];
    DATA.tech = data[5];
    DATA.misc = data[6];
    DATA.film = data[7];
    DATA.literature = data[8];

    document.getElementById("correctPercent").innerHTML = correctPercent + "%";
    document.getElementById("questionsAnswered").innerHTML = answeredQuestions;
    document.getElementById("gamesPlayed").innerHTML = playedGames;

    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);
}